from socket import *
import os
import time
import random
import string
import hashlib
import requests
from themis.checker import Server, Result


SERVICE_PORT = int(os.getenv('SERVICE_PORT', 2727))
SOCKET_TIMEOUT = int(os.getenv('SOCKET_TIMEOUT', 10))
CHECKER_HOST_IP = os.getenv('CHECKER_HOST_IP', '')


class TooMuchException(Exception):
    pass


class Snake_checker(Server):
	
	def read_until(self, fd, ch):
		l = len(ch)
		received_data = ''
		while True:
			received_data += fd.recv(1)
			if received_data[-l:] == ch:
				break
			if len(received_data) > 512:
				raise TooMuchException('The recieved data is too long')
		return received_data
	
	def get_checksum(self, flag):
		m = hashlib.md5()
		m.update(flag)
		return m.hexdigest()
		
	def get_flag_id(self):
		m = hashlib.md5()
		m.update(str(requests.get('http://{0}/api/contest/round'.format(CHECKER_HOST_IP)).json()['value']))
		return m.hexdigest()[:4]  + hashlib.md5(os.urandom(16)).hexdigest()[4:]
		
	def push(self, team_ip, flag_id, flag):
		#flag = flag.decode('utf-8').encode('ascii')
		flag_id = self.get_flag_id()

		try:
			conn = create_connection((str(team_ip), SERVICE_PORT))
			conn.settimeout(SOCKET_TIMEOUT)
		except Exception as ex:
			self.logger.error('Failed connect to the service')
			self.logger.debug(str(ex), exc_info=True)
			return (Result.DOWN, flag_id)
			
		try:
			for i in xrange(6):
				self.read_until(conn, '\n')
			conn.sendall("$play\n")
			self.read_until(conn, '\n')
			conn.sendall("'BREAK'\n")
			self.read_until(conn, '\n')
		except TooMuchException:
			self.logger.error('Too much data has already been read')
			return (Result.MUMBLE, flag_id)
		except Exception as ex:
			self.logger.error('Failed greeting and playing')
			self.logger.debug(str(ex), exc_info=True)
			return (Result.MUMBLE, flag_id)	
			
		try:
			conn.sendall("$push\n")
			conn.sendall("SamaraCTF2016#%s{%s}\n" % (flag_id[:4], flag))
			self.read_until(conn, '\n')
			true_chksum = self.read_until(conn, '\n')[19:51]
			if self.get_checksum(flag) != true_chksum:
				return (Result.MUMBLE, flag_id)
		except TooMuchException:
                        self.logger.error('Too much data has already been read')
                        return (Result.MUMBLE, flag_id)
		except Exception as ex:
			print ex
			self.logger.error('Failed to commit the protocol (though the service is responding)')
			self.logger.debug(str(ex), exc_info=True)
			return (Result.MUMBLE, flag_id)
		finally:
			conn.sendall("$stop\n")
			conn.close()
		return (Result.UP, flag_id)
		
	def pull(self, team_ip, flag_id, flag):
		#flag = flag.decode('utf-8').encode('ascii')

		try:
			conn = create_connection((str(team_ip), SERVICE_PORT))
			conn.settimeout(SOCKET_TIMEOUT)
		except Exception as ex:
			self.logger.error('Failed connect to the service')
			self.logger.debug(str(ex), exc_info=True)
			return Result.DOWN

		try:
			for i in xrange(6):
				self.read_until(conn, '\n')
			conn.sendall("$play\n")
			self.read_until(conn, '\n')
			conn.sendall("'BREAK'\n")
			self.read_until(conn, '\n')
		except TooMuchException:
                        self.logger.error('Too much data has already been read')
                        return Result.MUMBLE
		except Exception as ex:
			self.logger.error('Failed greeting and playing')
			self.logger.debug(str(ex), exc_info=True)
			return Result.MUMBLE
		try:
			conn.sendall("$pull\n")
			conn.sendall("SamaraCTF2016#%s{%s}\n" % (flag_id[:4], self.get_checksum(flag)) )
			#print "DEBUG: prtcl SamaraCTF2016#%s{%s}\n" % (flag_id[:4], self.get_checksum(flag)) 
			self.read_until(conn, '\n')
			if flag != self.read_until(conn, '\n')[19:52]:
				return Result.CORRUPT
		except TooMuchException:
                        self.logger.error('Too much data has already been read')
                        return Result.MUMBLE
		except Exception as ex:
			self.logger.error('Failed to commit the protocol (though the service is responding)')
			self.logger.debug(str(ex), exc_info=True)
			return Result.MUMBLE
		finally:		
			conn.sendall("$stop\n")
			conn.close()

		return Result.UP	


checker = Snake_checker()
checker.run()

"""team_ip = '212.24.97.112'
flag = '{0}='.format(hashlib.md5(os.urandom(16)).hexdigest())
(res, flag_id) = checker.push(team_ip, '', flag)
print 'Push result:', res
print 'Pull result: ', checker.pull(team_ip, flag_id, flag)"""
